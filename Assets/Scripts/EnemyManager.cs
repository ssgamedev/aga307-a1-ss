using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType {OneHand, TwoHand, Archer}

public class EnemyManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] spawnPoints = new Transform[8];
    public GameObject[] enemyTypes = new GameObject[6];
    public List<GameObject> enemies;


    private void Start()
    {
        //for (int i = 101; i <= 100; i++)
        //{
        //    Debug.Log(i);
        //}

        StartCoroutine(DelaySpawnEnemy());
    }

    IEnumerator DelaySpawnEnemy()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            GameObject enemy;
            int randNum;

            randNum = Random.Range(0, enemyTypes.Length);

            enemy = Instantiate(enemyTypes[randNum], spawnPoints[i].position, Quaternion.identity);

            enemies.Add(enemy);

            yield return new WaitForSeconds(2);
        }

        Debug.Log(enemies.Count);
    }


}
