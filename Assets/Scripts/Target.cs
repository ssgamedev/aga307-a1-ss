using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public TargetSize targetSize;
    //public Transform[] spawnPoints = new Transform[8];

    private void Start()
    {
        //StartCoroutine(Move());
        Setup();
    }
    void Setup()
    {
        switch (targetSize)
        {
            case TargetSize.Small:
                float scaleFactor = 0.5f;
                transform.localScale = Vector3.one * scaleFactor;
                GetComponent<Renderer>().material.color = Color.red;
                break;
            case TargetSize.Medium:
                scaleFactor = 1f;
                transform.localScale = Vector3.one * scaleFactor;
                GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case TargetSize.Large:
                scaleFactor = 2f;
                transform.localScale = Vector3.one * scaleFactor;
                GetComponent<Renderer>().material.color = Color.green;
                break;
            default:
                scaleFactor = 1f;
                transform.localScale = Vector3.one * scaleFactor;
                GetComponent<Renderer>().material.color = Color.white;
                break;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("r"))
        {
            StartCoroutine(SizeChange());
        }
    }



    IEnumerator SizeChange()
    {

        yield return null;
    }
}
