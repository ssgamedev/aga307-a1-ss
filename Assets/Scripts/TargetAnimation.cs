using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetAnimation : MonoBehaviour
{
    Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();   
    }

    private void OnCollisionEnter(Collision collision)
    {
        anim.SetTrigger("HitTarget");
    }
}
