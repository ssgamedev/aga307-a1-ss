using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    float moveDistance = 500;
    public int enemyHealth;

    public EnemyType myType;

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(Move());
        Setup();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TakeDamage(10);
        }
    }

    void TakeDamage(int _damage)
    {
        anim.SetTrigger("Hit");
        enemyHealth -= _damage;
        GameManager.instance.AddScore(15);
        if (enemyHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        anim.SetTrigger("Die");
        GameManager.instance.AddScore(100);
        StopAllCoroutines();
        Destroy(this.gameObject, 5);
    }

    void Setup()
    {
        switch (myType)
        {
            case EnemyType.OneHand:
                enemyHealth = 100;
                break;
            case EnemyType.TwoHand:
                enemyHealth = 200;
                break;
            case EnemyType.Archer:
                enemyHealth = 50;
                break;
            default:
                enemyHealth = 100;
                break;
        }
    }

    IEnumerator Move()
    {
        anim.SetTrigger("Walk");
        for (int i = 0; i < moveDistance; i++)
        {
            transform.Translate(Vector3.forward * Time.deltaTime);
            yield return null;
        }

        transform.Rotate(Vector3.up * 180);

        anim.SetTrigger("Idle");

        yield return new WaitForSeconds(3f);

        StartCoroutine(Move());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            TakeDamage(15);
        }
    }

}
