using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Transform positionToSpawn;
    public Rigidbody projectileRigidBody;

    public GameObject weapon0;
    public GameObject weapon1;
    public GameObject weapon2;
    public GameObject gunObj;

    public float bulletSpeed;
    //public float bulletPosition;
    public GameObject currentWeapon;
    public float shotgunPellets;
    public float shotgunSpread = 0.1f;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject projectileInstance;

            if (currentWeapon == weapon1)
            {
                for (int i = 0; i < shotgunPellets; i++)
                {
                    //var pelletRotX = Random.Range(-shotgunSpread, shotgunSpread);
                    //var pelletRotY = Random.Range(-shotgunSpread, shotgunSpread);
                    //var shotgunInstance = Instantiate(weapon2, transform.position, transform.rotation);
                    //shotgunInstance.transform.Rotate(pelletRotX, pelletRotY, 0.0f);
                    //Rigidbody shotgunRigidBody = shotgunInstance.GetComponent<Rigidbody>();
                    //shotgunRigidBody.AddForce(positionToSpawn.forward * bulletSpeed);

                    //var bulletPosition = transform.rotation;

                    //bulletPosition.y += Random.Range(0, 10);

                    //bulletPosition.x += Random.Range(0, 10);

                    projectileInstance = Instantiate(currentWeapon, positionToSpawn.position, Quaternion.identity);

                    //Rigidbody projectileRigidBody = projectileInstance.GetComponent<Rigidbody>();

                    //projectileRigidBody.AddForce(positionToSpawn.forward * bulletSpeed);

                    Vector3 dir = transform.forward + new Vector3(Random.Range(-shotgunSpread, shotgunSpread), Random.Range(-shotgunSpread, shotgunSpread), Random.Range(-shotgunSpread, shotgunSpread));
                    projectileInstance.GetComponent<Rigidbody>().AddForce(dir * bulletSpeed);

                }
                //bulletPosition = 0;
            }

            else 
            { 
                projectileInstance = Instantiate(currentWeapon, positionToSpawn.position, Quaternion.identity);

                Rigidbody projectileRigidBody = projectileInstance.GetComponent<Rigidbody>();

                projectileRigidBody.AddForce(positionToSpawn.forward * bulletSpeed);
            }

            //Destroy(GameObject.FindWithTag("Projectile"), 0.5f);
        }

        Destroy(GameObject.FindWithTag("Projectile"), 0.5f);

        if (Input.GetButtonDown("1"))
        {
            currentWeapon = weapon0;
            Debug.Log("Pistol");
            bulletSpeed = 500;
        }

        if (Input.GetButtonDown("2"))
        {
            currentWeapon = weapon1;
            Debug.Log("Shotgun");
            bulletSpeed = 1000;
        }

        if (Input.GetButtonDown("3"))
        {
            currentWeapon = weapon2;
            Debug.Log("Sniper");
            bulletSpeed = 2000;
        }

    }
}
