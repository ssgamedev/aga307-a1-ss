using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : Singleton<UIManager>
{
    public TMP_Text scoreText;
    public TMP_Text timerText;
    public TMP_Text enemiesCount;
    public TMP_Text difficultyDisplay;
    public TMP_Text weaponText;
}
