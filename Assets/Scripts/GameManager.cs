using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public enum GameState {Start, Playing, Paused, GameOver}

public enum Difficulty {Easy, Medium, Hard}
public class GameManager : Singleton<GameManager>
{
    public GameState gameState;
    public Difficulty difficulty;
    public bool playing = true;
    int scoreMultiplier;
    int score;
    //public float startTime = 30;
    public float currentTime = 30;

    // Start is called before the first frame update
    void Start()
    {
        gameState = GameState.Start;
        difficulty = Difficulty.Easy;
        Setup();
        Debug.Log("Score Multiplier = " + scoreMultiplier);

    }

    public void AddScore(int _points)
    {
        //scoreMultiplier += _points * scoreMultiplier;
        score += _points * scoreMultiplier;
        UIManager.instance.scoreText.text = "Score: " + score;
    }

    void Setup()
    {
        switch (difficulty)
        {
            case Difficulty.Easy:
                scoreMultiplier = 1;
                break;
            case Difficulty.Medium:
                scoreMultiplier = 2;
                break;
            case Difficulty.Hard:
                scoreMultiplier = 3;
                break;
            default:
                scoreMultiplier = 1;
                break;
        }
    }

    private void Update()
    {
        int enemies = GameObject.FindGameObjectsWithTag("Enemy").Length;

        currentTime -= Time.deltaTime;

        UIManager.instance.timerText.text = "Time: " + currentTime.ToString("F3");

        UIManager.instance.difficultyDisplay.text = "Difficulty: " + difficulty;

        UIManager.instance.enemiesCount.text = "Enemies Left: " + enemies;

        if (Input.GetKeyDown("1"))
        {
            UIManager.instance.weaponText.text = "Weapon: Pistol";
        }
        if (Input.GetKeyDown("2"))
        {
            UIManager.instance.weaponText.text = "Weapon: Shotgun";
        }
        if (Input.GetKeyDown("3"))
        {
            UIManager.instance.weaponText.text = "Weapon: Sniper";
        }
    }
}
