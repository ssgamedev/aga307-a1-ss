using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringPoint : MonoBehaviour
{
    void FixedUpdate()
    {
        RaycastHit hit;

        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        if (Input.GetButtonDown("Fire1"))
        {
            if (Physics.Raycast(transform.position, fwd, out hit, 10))
            {
                print(hit.collider.name);

                if (hit.collider.CompareTag("Target"))
                {
                    hit.collider.GetComponent<Renderer>().material.color = Color.blue;
                }
            }
        }
    }
}
