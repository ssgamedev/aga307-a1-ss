using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float crouchSpeed = 4f;
    public float sprintSpeed = 20f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float defaultFov = 60f;
    public float sprintFov = 65f;
    public float m_FieldOfView;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        Camera.main.fieldOfView = m_FieldOfView;

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if(Input.GetButton("Sprint"))
        {
            speed = sprintSpeed;
            if (m_FieldOfView < sprintFov)
            {
                m_FieldOfView += Mathf.Lerp(0, 1, Time.deltaTime * 50f);
            }
            else
            {
                m_FieldOfView = sprintFov;
            }
        }
        else
        {
            speed = 12f;
            m_FieldOfView = defaultFov;
        }
        if (Input.GetButton("Crouch"))
        {
            controller.height = 1.0f;
            speed -= Mathf.Lerp(1, crouchSpeed, Time.deltaTime * 100f);
        }
        else
        {
            controller.height = 2.0f;
        }
    }
}
