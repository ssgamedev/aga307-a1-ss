using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTarget : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Projectile"))
        {
            collision.collider.GetComponent<Renderer>().material.color = Color.red;

            Destroy(collision.collider.gameObject, 1f);

            Destroy(this.gameObject);
        }
    }
}
