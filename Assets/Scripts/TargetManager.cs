using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TargetSize {Small, Medium, Large}

public class TargetManager : MonoBehaviour
{
    public float moveSpeed = 500;
    public GameObject[] targetTypes = new GameObject[3];
    public List<GameObject> targets;
    public Transform[] spawnPoints = new Transform[8];
    public GameObject[] currentTargets;

    private void Start()
    {
        currentTargets = GameObject.FindGameObjectsWithTag("Target");
        StartCoroutine(Move());
    }

    void Update()
    {
        if (Input.GetButtonDown("i"))
        {
            SpawnTarget();
        }
    }

    public void SpawnTarget()
    {
        GameObject target;

        int randNum = Random.Range(0, targetTypes.Length);

        target = Instantiate(targetTypes[randNum], spawnPoints[randNum].position, Quaternion.identity);

        targets.Add(target);

        Debug.Log(targets.Count);
    }

    IEnumerator Move()
    {
        foreach (GameObject obj in currentTargets)
        {
            int randNum = Random.Range(0, targetTypes.Length);

            obj.transform.position = spawnPoints[randNum].position;
        }


        //Debug.Log("Moved");

        yield return new WaitForSeconds(2);

        StartCoroutine(Move());

    }
}
